package org.coderearth.apps.todos.web.controllers;

import org.coderearth.apps.todos.services.GreeterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by kunal_patel on 23/10/15.
 */
@RestController
public class GreeterController {

	public GreeterController(){
		System.out.println("Loading GreeterController()");
		System.out.println("Finished loading GreeterController()");
	}

    @Autowired
    private GreeterService greeterService;

	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public @ResponseBody String greet() {
        return greeterService.greet("world");
    }

}
