package org.coderearth.apps.todos.services;

import org.springframework.stereotype.Service;


@Service
public class GreeterService {

	public GreeterService(){
		System.out.println("Loading GreeterService()");
		System.out.println("Finished loading GreeterService()");
	}

    public String greet(String name) {
        return String.format("%s %s\n", "Hello", name);
    }
}
