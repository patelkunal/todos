package org.coderearth.apps.todos.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * Created by kunal_patel on 23/10/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/applicationContext.xml")
public class GreeterServiceTest {

	@Autowired
	private GreeterService greeterService;

	@Test
	public void testGreet() throws Exception {
		assertNotNull(greeterService);
		assertEquals("Hello world", greeterService.greet("world"));
		assertEquals("Hello Kunal", greeterService.greet("Kunal"));

	}
}